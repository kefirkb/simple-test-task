import {Component, OnInit} from "@angular/core";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {


  ngOnInit():void {
  }

  constructor(private confirmationService:ConfirmationService) {
  }

  confirm() {
    this.confirmationService.confirm({
      message: 'Are you sure to logout?',
      accept: () => {
        window.open(window.location.origin, '_self').close();
        //window.parent.close();
      }
    });
  }

}
