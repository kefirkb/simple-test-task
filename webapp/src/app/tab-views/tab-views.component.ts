import {Component, OnInit, ViewChild} from "@angular/core";
import {TableComponent} from "../table/table.component";
import {OtherTableComponent} from "../other-table/other-table.component";
import {Exportable} from "../utils/Exportable";

@Component({
  selector: 'app-tab-views',
  templateUrl: './tab-views.component.html',
  styleUrls: ['./tab-views.component.css']
})
export class TabViewsComponent implements OnInit {

  DEFAULT_HEADER = 'ISP National APHT Report';

  fileName:string;
  selectedTabIndex:number;
  displayDialog:boolean;

  @ViewChild(TableComponent)
  tableOne:TableComponent;

  @ViewChild(OtherTableComponent)
  tableTwo:OtherTableComponent;

  tableComponents:Exportable[];

  constructor() {
  }

  ngOnInit() {
    this.displayDialog = false;
    this.fileName = this.DEFAULT_HEADER + '<' + new Date().toDateString() + '>';
    this.selectedTabIndex = 0;
    this.tableComponents = [this.tableOne, this.tableTwo];
  }

  selectTab(e) {
    this.selectedTabIndex = e.index;
    this.getCurrentTable(this.selectedTabIndex).update();
  }

  showDialog() {
    this.displayDialog = true;
  }

  confirmExport() {
    this.getCurrentTable(this.selectedTabIndex).exportCSV(this.fileName);
    this.displayDialog = false;
  }

  private getCurrentTable(index:number) {
    return this.tableComponents[this.selectedTabIndex];
  }

  cancelExport() {
    this.displayDialog = false;
  }

}
