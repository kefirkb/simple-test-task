import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {CommonModule, LocationStrategy, HashLocationStrategy} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {
  DataTableModule,
  SharedModule,
  PaginatorModule,
  MenuModule,
  ButtonModule,
  MessagesModule,
  GrowlModule,
  TabViewModule,
  AutoCompleteModule,
  DialogModule,
  BlockUIModule,
  SplitButtonModule,
  ConfirmDialogModule,
  ConfirmationService
} from "primeng/primeng";
import "rxjs/add/operator/toPromise";
import {PaginationModule, TabsModule} from "ng2-bootstrap/ng2-bootstrap";
import {TableComponent} from "./table/table.component";
import {FirstEntityService} from "./services/FirstEntityService";
import {MenuButtonComponent} from "./menu-button/menu-button.component";
import {RouterModule, Routes} from "@angular/router";
import {TabViewsComponent} from "./tab-views/tab-views.component";
import {OtherTableComponent} from "./other-table/other-table.component";
import {SecondEntityService} from "./services/SecondEntityService";
import {LogoutComponent} from "./logout/logout.component";


export const appRoutes:Routes = [
  {path: '*', component: AppComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    MenuButtonComponent,
    MenuButtonComponent,
    TabViewsComponent,
    OtherTableComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    PaginationModule,
    TabsModule,
    DataTableModule,
    PaginatorModule,
    MenuModule,
    MessagesModule,
    SharedModule,
    ButtonModule,
    GrowlModule,
    AutoCompleteModule,
    TabViewModule,
    CommonModule,
    DialogModule,
    SplitButtonModule,
    BlockUIModule,
    ConfirmDialogModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [FirstEntityService, SecondEntityService, ConfirmationService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
