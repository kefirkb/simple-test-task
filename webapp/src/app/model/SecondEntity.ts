/**
 * Created by sergey on 30.01.17.
 */
export class SecondEntity {
  id: number;
  name: string;
  date:Date;
  active: boolean;
}
