/**
 * Created by sergey on 30.01.17.
 */
export class FirstEntity {
  id:number;
  name:string;
  code:string;
  percents:number;
  count:number;
  order:number;
  priority:number;
}
