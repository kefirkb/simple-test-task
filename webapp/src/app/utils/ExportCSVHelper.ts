export class ExportCSVHelper {

  public static exportCSV(filename:string, columns:any[], entities:any[]) {
    let csvSeparator = ';';
    let csv = '';

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].field) {
        csv += columns[i].field;

        if (i < (columns.length - 1)) {
          csv += csvSeparator;
        }
      }
    }

    entities.forEach((record, j) => {
      csv += '\n';
      for (let i = 0; i < columns.length; i++) {
        if (columns[i].field) {
          console.log(record[columns[i].field]);

          csv += record[columns[i].field];

          if (i < (columns.length - 1)) {
            csv += csvSeparator;
          }
        }
      }
    });
    this.download(csv, filename);
  }

  private static download(text, filename) {
    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
    element.style.display = 'none';

    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
}
