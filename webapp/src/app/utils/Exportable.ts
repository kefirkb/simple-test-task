export interface Exportable {
  exportCSV(fileName:string);
  update();
}
