import {Component, OnInit} from "@angular/core";
import {FirstEntity} from "../model/FirstEntity";
import {FirstEntityService} from "../services/FirstEntityService";
import {ExportCSVHelper} from "../utils/ExportCSVHelper";
import {Exportable} from "../utils/Exportable";

@Component({
  selector: 'app-table-component',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, Exportable {

  entities: FirstEntity[];

  /*
   Я сделал это поле статическим потому что когда дергается метод
   rowColoredClass почему то все содержимое компонента становится undefined;
   */
  static selectedEntity:FirstEntity;

  columns:any[];

  blocked:boolean;


  constructor(private firstEntityService: FirstEntityService) {
  }

  ngOnInit() {
    this.blocked = false;
    this.columns = [
      {field: 'name', header: 'Name'},
      {field: 'code', header: 'Code'},
      {field: 'percents', header: 'Percents'},
      {field: 'count', header: 'Count'},
      {field: 'order', header: 'Order'},
      {field: 'priority', header: 'Priority'},
    ];
    this.update();
  }

  update() {
    this.blockUI();
    this.firstEntityService.getEntities().then(entities => {
      this.unBlockUI();
      this.entities = entities
    });
  }

  blockUI() {
    this.blocked = true;
  }

  unBlockUI() {
    this.blocked = false;
  }

  rowColoredClass(entity: FirstEntity) {

    if (TableComponent.selectedEntity && TableComponent.selectedEntity === entity) {
      return '';
    }

    if (entity.percents > 70 && entity.percents < 84) {
      return 'red-background';
    }

    if (entity.percents > 50 && entity.percents < 70) {
      return 'green-background';
    }
    return '';
  }

  onRowSelect(event) {
    TableComponent.selectedEntity = event.data;
  }

  exportCSV(fileName:string) {
    ExportCSVHelper.exportCSV(fileName, this.columns, this.entities);
  }

}
