import {Component, OnInit} from "@angular/core";
import {SecondEntityService} from "../services/SecondEntityService";
import {SecondEntity} from "../model/SecondEntity";
import {Exportable} from "../utils/Exportable";
import {ExportCSVHelper} from "../utils/ExportCSVHelper";

@Component({
  selector: 'app-other-table',
  templateUrl: './other-table.component.html',
  styleUrls: ['./other-table.component.css']
})
export class OtherTableComponent implements OnInit, Exportable {

  entities: SecondEntity[];
  selectedEntity: SecondEntity;
  columns:any[];
  blocked:boolean;

  constructor(private secondEntityService: SecondEntityService) {
    this.columns = [
      {field: 'id', header: 'Id'},
      {field: 'name', header: 'Name'},
      {field: 'date', header: 'Date'},
      {field: 'active', header: 'Active'},
    ];
    this.blocked = false;
  }

  ngOnInit() {
    this.update();
  }

  update() {
    this.blockUI();
    this.secondEntityService.getEntities().then(entities => {
      this.unBlockUI();
      this.entities = entities
    });
  }

  blockUI() {
    this.blocked = true;
  }

  unBlockUI() {
    this.blocked = false;
  }

  exportCSV(fileName:string) {
    ExportCSVHelper.exportCSV(fileName, this.columns, this.entities);
  }

}
