/**
 * Created by sergey on 30.01.17.
 */
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {SecondEntity} from "../model/SecondEntity";

@Injectable()
export class SecondEntityService {

  constructor(private http: Http) {
  }

  getEntities() {
    return this.http.get('http://localhost:9000/second')
      .toPromise()
      .then(res => <SecondEntity[]> res.json())
      .then(data => {
        return data;
      });
  }
}
