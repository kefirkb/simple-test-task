/**
 * Created by sergey on 30.01.17.
 */
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {FirstEntity} from "../model/FirstEntity";

@Injectable()
export class FirstEntityService {

  constructor(private http:Http) {
  }

  getEntities() {
    return this.http.get('http://localhost:9000/first')
      .toPromise()
      .then(res => <FirstEntity[]> res.json())
      .then(data => {
        return data;
      });
  }
}
