import {Component, OnInit} from "@angular/core";
import {Message, MenuItem} from "primeng/primeng";


@Component({
  selector: 'app-menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.css']
})
export class MenuButtonComponent implements OnInit {

  items:MenuItem[];
  msgs:Message[] = [];

  ngOnInit() {
    this.items = [
      {
        label: 'MenuItem1',
        command: () => {
          this.menuItem1ClickHandler();
        }
      },
      {
        label: 'MenuItem2',
        command: () => {
          this.menuItem2ClickHandler();
        }
      },
      {
        label: 'MenuItem3',
        command: () => {
          this.menuItem3ClickHandler();
        }
      }
    ];
  }

  menuItem1ClickHandler() {
    this.msgs = [];
    this.msgs.push({severity: 'info', summary: 'Success', detail: 'clicked on menu item 1'});
  }

  menuItem2ClickHandler() {
    this.msgs = [];
    this.msgs.push({severity: 'info', summary: 'Success', detail: 'clicked on menu item 2'});
  }

  menuItem3ClickHandler() {
    this.msgs = [];
    this.msgs.push({severity: 'info', summary: 'Success', detail: 'clicked on menu item 3'});
  }
}
