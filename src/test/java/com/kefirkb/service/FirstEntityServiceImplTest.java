package com.kefirkb.service;

import com.kefirkb.model.FirstEntity;
import com.kefirkb.repository.FirstEntityRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FirstEntityServiceImplTest {

    @Mock
    private FirstEntityRepository repository;

    @InjectMocks
    private FirstEntityServiceImpl service;

    @Test
    public void findAllFirstEntity() throws Exception {
        when(repository.findAllFirstEntity()).thenReturn(buildListFirstEntity());
        List<FirstEntity> result = service.findAllFirstEntity();
        assertEquals(result.size(), 1);
        verify(repository ,times(1)).findAllFirstEntity();
    }

    private List<FirstEntity> buildListFirstEntity() {
        return singletonList(FirstEntity
                .builder()
                .name("name1")
                .code("aaa111")
                .build());
    }

}