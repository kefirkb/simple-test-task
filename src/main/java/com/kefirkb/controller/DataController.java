package com.kefirkb.controller;

import com.kefirkb.model.FirstEntity;
import com.kefirkb.model.SecondEntity;
import com.kefirkb.service.FirstEntityService;
import com.kefirkb.service.SecondEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by kefirkb on 30.01.2017.
 */

@CrossOrigin
@RestController
public class DataController {

    @Autowired
    private FirstEntityService firstEntityService;

    @Autowired
    private SecondEntityService secondEntityService;

    @RequestMapping(value = "/first", method = GET)
    @ResponseBody
    public List<FirstEntity> getFirst() throws IOException {
        return firstEntityService.findAllFirstEntity();
    }

    @RequestMapping(value = "/second", method = GET)
    @ResponseBody
    public List<SecondEntity> getSecond() throws IOException, InterruptedException {
        //sleep(5000);
        return secondEntityService.findAllSecondEntity();
    }
}