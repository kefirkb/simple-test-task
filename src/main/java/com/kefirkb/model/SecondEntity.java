package com.kefirkb.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SecondEntity {
    private Integer id;
    private String name;
    private String date;
    private Boolean active;
}
