package com.kefirkb.service;

import com.kefirkb.model.SecondEntity;
import com.kefirkb.repository.SecondEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class SecondEntityServiceImpl implements SecondEntityService {
    @Autowired
    private SecondEntityRepository repository;

    @Override
    public List<SecondEntity> findAllSecondEntity() throws IOException {
        return repository.findAllSecondEntity();
    }
}
