package com.kefirkb.service;

import com.kefirkb.model.FirstEntity;

import java.io.IOException;
import java.util.List;

public interface FirstEntityService {
    List<FirstEntity> findAllFirstEntity() throws IOException;
}
