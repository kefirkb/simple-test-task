package com.kefirkb.service;

import com.kefirkb.model.SecondEntity;

import java.io.IOException;
import java.util.List;

public interface SecondEntityService {
    List<SecondEntity> findAllSecondEntity() throws IOException;
}
