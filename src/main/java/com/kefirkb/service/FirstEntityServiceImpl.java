package com.kefirkb.service;

import com.kefirkb.model.FirstEntity;
import com.kefirkb.repository.FirstEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class FirstEntityServiceImpl implements FirstEntityService {

    @Autowired
    private FirstEntityRepository repository;

    @Override
    public List<FirstEntity> findAllFirstEntity() throws IOException {
        return repository.findAllFirstEntity();
    }
}
