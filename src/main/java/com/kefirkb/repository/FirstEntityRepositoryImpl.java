package com.kefirkb.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kefirkb.model.FirstEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Repository
public class FirstEntityRepositoryImpl implements FirstEntityRepository {

    @Value("${first_file.path}")
    private String filePath;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public List<FirstEntity> findAllFirstEntity() throws IOException {
        return mapper.readValue(new File(filePath), new TypeReference<List<FirstEntity>>() {
        });
    }
}
