package com.kefirkb.repository;

import com.kefirkb.model.SecondEntity;

import java.io.IOException;
import java.util.List;

public interface SecondEntityRepository {
    List<SecondEntity> findAllSecondEntity() throws IOException;
}
