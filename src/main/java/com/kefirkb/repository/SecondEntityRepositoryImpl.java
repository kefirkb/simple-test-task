package com.kefirkb.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kefirkb.model.SecondEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Repository
public class SecondEntityRepositoryImpl implements SecondEntityRepository {
    @Value("${second_file.path}")
    private String filePath;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public List<SecondEntity> findAllSecondEntity() throws IOException {
        return mapper.readValue(new File(filePath), new TypeReference<List<SecondEntity>>() {
        });
    }
}
