package com.kefirkb.repository;

import com.kefirkb.model.FirstEntity;

import java.io.IOException;
import java.util.List;

public interface FirstEntityRepository {
    List<FirstEntity> findAllFirstEntity() throws IOException;
}
